# Rule Engine Demo

## Intro
    This demo repo shows how to implement the Nools rule engine into a static Hugo site. The rule engine in place will test content rendered in Hugo, to see if all content is correctly displayed as expected.

## Requirements
* Nools 
* Node
* npm
* Webpack

## QA
* If any content file contains "Lorem", return name of file in console.
* TBD